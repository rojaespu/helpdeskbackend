const pool = require('../db')

const getAllTasks = async (req, res, next) => {
    try {
        // throw new Error('algo salio mal rolando')
        const allTasks = await pool.query('select * from tickets')
        res.json(allTasks.rows);
    } catch (error) {
        next(error);
    }
}

const getTask = async (req, res, next) => {
    try {
        const {id} = req.params
        const result = await pool.query('select * from tickets where id = $1 order by id',[id])
        if (result.rows.length === 0) return res.status(204).json({
            message: 'Task not found'
        })
        res.json(result.rows[0]);
    } catch (error) {
        next(error);
    }
}

const createTask = async (req, res, next) => {
    const {nombre, descripcion, prioridad} = req.body;
   
    try {
        const result = await pool.query(
            'INSERT INTO public.tickets ( nombre, descripcion, prioridad) VALUES( $1, $2, $3) returning *',
            [nombre, descripcion, prioridad]
        )                
        res.json(result.rows[0]);
    } catch (error) {
        next(error);        
    }
}

const deleteTask = async (req, res, next) => {
    try {
        const {id} = req.params
        const result = await pool.query('delete from tickets where id = $1',[id])
        console.log(result)
        if (result.rows.length === 0) return res.status(204).json({
            message: 'Task not found'
        })
        return res.sendStatus();
    } catch (error) {
        next(error);
    }
}

const updateTask = async (req, res, next) => {
    try {
        const {id} = req.params
        const { nombre } = req.body;
        const { descripcion } = req.body;
        const { prioridad } = req.body;
        const { tecnico } = req.body;

        if ( !tecnico){
            const result = await pool.query('update tickets set nombre= $1, descripcion= $2, prioridad = $3 where id = $4',
                                        [nombre, descripcion, prioridad, id])
        } else {
            const result = await pool.query('update tickets set nombre= $1, descripcion= $2, prioridad = $3, tecnico = $5, fecha_asignacion = now() where id = $4',
                                        [nombre, descripcion, prioridad, id, tecnico])
        }

        if (result.rows.length === 0) return res.status(204).json({
            message: 'Task not found'
        })
        return res.json(result.rows[0]);
    } catch (error) {
        next(error);
    }
}

const updateTaskAprobar = async (req, res, next) => {
    try {
        const {id} = req.params
        // const {id_aux} = req.body;
        // console.log('rolando aprobar--> ' + id)
        // console.log('rolando aprobar--> ' + id_aux)
        const result = await pool.query('update tickets set estado= true, fecha_aprobacion= now() where id = $1',
                                        [id])
        if (result.rows.length === 0) return res.status(204).json({
            message: 'Task not found'
        })
        return res.json(result.rows[0]);
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAllTasks,
    getTask,
    createTask,
    deleteTask,
    updateTask,
    updateTaskAprobar
}