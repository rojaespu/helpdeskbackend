const {Pool} = require('pg')
const {db} = require('./config')

// const pool = new Pool({
//     user: 'openpg',
//     password: '12345',
//     host: 'localhost',
//     port: 5432,
//     database:'HelpDesk'
// })

const pool = new Pool({
    user: db.user,
    password: db.password,
    host: db.host,
    port: db.port,
    database: db.database
})


module.exports = pool